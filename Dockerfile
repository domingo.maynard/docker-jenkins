FROM ubuntu:22.04

RUN apt-get update && apt-get install -y wget && rm -rf /var/lib/apt/lists/*

# Jenkins and Java installation part
RUN wget -O /usr/share/keyrings/jenkins-keyring.asc \
    https://pkg.jenkins.io/debian-stable/jenkins.io-2023.key
RUN echo deb [signed-by=/usr/share/keyrings/jenkins-keyring.asc] \
    https://pkg.jenkins.io/debian-stable binary/ | tee \
    /etc/apt/sources.list.d/jenkins.list > /dev/null

RUN apt-get update
RUN apt-get install -y fontconfig openjdk-17-jre python3 python3-pip util-linux
RUN apt-get install -y jenkins

# Terraform and Git installation part
RUN apt update
RUN apt install -y git

# AWS CLI
RUN apt install unzip
RUN curl "https://awscli.amazonaws.com/awscli-exe-linux-aarch64.zip" -o "awscliv2.zip"
RUN unzip awscliv2.zip
RUN ./aws/install --update

RUN apt install -y curl software-properties-common
RUN wget -O- https://apt.releases.hashicorp.com/gpg | gpg --dearmor -o /usr/share/keyrings/hashicorp-archive-keyring.gpg
RUN echo "deb [signed-by=/usr/share/keyrings/hashicorp-archive-keyring.gpg] https://apt.releases.hashicorp.com $(lsb_release -cs) main" | tee /etc/apt/sources.list.d/hashicorp.list
RUN sed -i 's/HTTP_PORT=8080/HTTP_PORT=8081/' /etc/default/jenkins

RUN apt update && apt install -y terraform
RUN systemctl enable jenkins
#RUN systemctl start jenkins